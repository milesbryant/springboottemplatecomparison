package uk.co.crunch.rest;

import config.ThymeLeafConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.Date;

@Service
@Import(ThymeLeafConfig.class)
public class ThymeleafTemplateService {

    @Autowired
    private TemplateEngine thymeleafTemplateEngine;
    
    public Context getContext() {
        Context context = new Context();
        
        context.setVariable("yearEndDocument", new YearEndDocument());
        
        
        return context;
    }    
    

    public String process() {
        return thymeleafTemplateEngine.process("thymeleaf.html", getContext());        
    }
    
    public static class YearEndDocument {
        public String registrationNumber = "12345678";

        public String companyName = "My Company";
        public Date yearEndDate = new Date(2016, 6, 8);
        public Date comparativeEndDate = new Date(2015, 4, 5);
        
        public IncomeStatement incomeStatement = new IncomeStatement();
        
        
        public static class IncomeStatement {
            public YearEndItem turnover = new YearEndItem(100030, 30000);
            public YearEndItem otherIncome = new YearEndItem(3000, 4000);
            public YearEndItem costOfRawMaterials = new YearEndItem(500, 3000);
            public YearEndItem staffCosts = new YearEndItem(2504, 2319);
            public YearEndItem depreciationAndOther = new YearEndItem(32, 56);
            public YearEndItem otherCharges = new YearEndItem(156, 81);
            public YearEndItem tax = new YearEndItem(800, 1400);
            public YearEndItem profit = new YearEndItem(450, 320);
        }
        
        public static class YearEndItem {
            public int balance, comparativeBalance;

            public YearEndItem(int balance, int comparativeBalance) {
                this.balance = balance;
                this.comparativeBalance = comparativeBalance;
            }

            public YearEndItem() {
            }
        }
        
    }
}
