package uk.co.crunch.rest;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uk.co.crunch.IncomeStatement;
import uk.co.crunch.YearEndDocument;
import uk.co.crunch.YearEndItem;

import java.io.IOException;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class FreeMarkerTemplateService {

    @Autowired
    private Configuration freeMarkerConfiguration;

    public String process() {
        String out;
        try {
            Template template = freeMarkerConfiguration.getTemplate("freemarker/master-ch.ftl");

            Map<String, Object> root = new HashMap<>();
            YearEndDocument yearEndDocument = new YearEndDocument();
            yearEndDocument.setCompanyName("Acme Ltd");
            yearEndDocument.setCompanyRegistrationNumber("12342345235");
            LocalDate ld = LocalDate.now().minusMonths(1L);
            Instant instant = ld.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
            Date date = Date.from(instant);
            yearEndDocument.setYearEndDate(date);
            ld = ld.minusYears(1L);
            instant = ld.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
            date = Date.from(instant);
            yearEndDocument.setComparativeEndDate(date);

            YearEndItem yearEndItem = new YearEndItem();
            IncomeStatement incomeStatement = new IncomeStatement();

            yearEndItem.setBalance(new BigDecimal("100.00"));
            yearEndItem.setComparativeBalance(new BigDecimal("150.00"));
            incomeStatement.setTax(yearEndItem);

            yearEndItem = new YearEndItem();
            yearEndItem.setBalance(new BigDecimal("200.00"));
            yearEndItem.setComparativeBalance(new BigDecimal("250.00"));
            incomeStatement.setCostOfRawMaterials(yearEndItem);

            yearEndItem = new YearEndItem();
            yearEndItem.setBalance(new BigDecimal("300.00"));
            yearEndItem.setComparativeBalance(new BigDecimal("-350.00"));
            incomeStatement.setDepreciationAndOther(yearEndItem);

            yearEndItem = new YearEndItem();
            yearEndItem.setBalance(new BigDecimal("0.00"));
            yearEndItem.setComparativeBalance(new BigDecimal("-450.00"));
            incomeStatement.setOtherIncome(yearEndItem);

            yearEndItem = new YearEndItem();
            yearEndItem.setBalance(new BigDecimal("500.00"));
            yearEndItem.setComparativeBalance(new BigDecimal("550.00"));
            incomeStatement.setProfit(yearEndItem);

            yearEndItem = new YearEndItem();
            yearEndItem.setBalance(new BigDecimal("600.00"));
            yearEndItem.setComparativeBalance(new BigDecimal("650.00"));
            incomeStatement.setTurnOver(yearEndItem);

            yearEndItem = new YearEndItem();
            yearEndItem.setBalance(new BigDecimal("700.00"));
            yearEndItem.setComparativeBalance(new BigDecimal("750.00"));
            incomeStatement.setStaffCosts(yearEndItem);

            yearEndItem = new YearEndItem();
            yearEndItem.setBalance(new BigDecimal("800.00"));
            yearEndItem.setComparativeBalance(new BigDecimal("850.00"));
            incomeStatement.setOtherCharges(yearEndItem);

            yearEndDocument.setIncomeStatement(incomeStatement);

            root.put("yearEndDocument", yearEndDocument);
            root.put("today", new Date());
            StringWriter templateOutput = new StringWriter();
            template.process(root, templateOutput);

            out = templateOutput.toString();
        } catch (TemplateException | IOException e) {
            throw new RuntimeException(e);
        }
        return out;
    }

}
