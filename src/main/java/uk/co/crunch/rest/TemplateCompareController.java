package uk.co.crunch.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Controller
public class TemplateCompareController {
    
    @Autowired
    private TemplateEngine thymeleafTemplateEngine;
    
    @Autowired
    private ThymeleafTemplateService thymeleafTemplateService;
    
    @Autowired
    private FreeMarkerTemplateService freeMarkerTemplateService;
    
    @RequestMapping("/")
    @ResponseBody
    public String index() {
        Context context = new Context();
        return thymeleafTemplateEngine.process("index", context);
    }
    
    @RequestMapping("/thymeleaf")
    @ResponseBody
    public String thymeleaf() {
        return thymeleafTemplateService.process();
    }

    @RequestMapping("/freemarker")
    @ResponseBody
    public String freeMarker() {
        return freeMarkerTemplateService.process();
    }
}
