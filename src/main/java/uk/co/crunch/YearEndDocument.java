package uk.co.crunch;

import java.util.Date;

/**
 * Created by jgroth on 13/04/16.
 */
public class YearEndDocument {

    private String companyRegistrationNumber;
    private String companyName;
    private Date yearEndDate;
    private Date yearStartDate;
    private Date comparativeEndDate;
    private Date comparativeStartDate;
    private IncomeStatement incomeStatement;

    public Date getYearStartDate() {
        return yearStartDate;
    }

    public void setYearStartDate(Date yearStartDate) {
        this.yearStartDate = yearStartDate;
    }

    public Date getComparativeEndDate() {
        return comparativeEndDate;
    }

    public void setComparativeEndDate(Date comparativeEndDate) {
        this.comparativeEndDate = comparativeEndDate;
    }

    public Date getComparativeStartDate() {
        return comparativeStartDate;
    }

    public void setComparativeStartDate(Date comparativeStartDate) {
        this.comparativeStartDate = comparativeStartDate;
    }

    public String getCompanyRegistrationNumber() {
        return companyRegistrationNumber;
    }

    public void setCompanyRegistrationNumber(String companyRegistrationNumber) {
        this.companyRegistrationNumber = companyRegistrationNumber;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Date getYearEndDate() {
        return yearEndDate;
    }

    public void setYearEndDate(Date yearEndDate) {
        this.yearEndDate = yearEndDate;
    }

    public IncomeStatement getIncomeStatement() {
        return incomeStatement;
    }

    public void setIncomeStatement(IncomeStatement incomeStatement) {
        this.incomeStatement = incomeStatement;
    }
}
