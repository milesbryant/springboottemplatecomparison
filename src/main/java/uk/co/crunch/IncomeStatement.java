package uk.co.crunch;

/**
 * Created by jgroth on 14/04/16.
 */
public class IncomeStatement {
    YearEndItem costOfRawMaterials;
    YearEndItem depreciationAndOther;
    YearEndItem otherIncome;
    YearEndItem tax;
    YearEndItem profit;
    YearEndItem turnOver;
    YearEndItem staffCosts;
    YearEndItem otherCharges;

    public YearEndItem getCostOfRawMaterials() {
        return costOfRawMaterials;
    }

    public void setCostOfRawMaterials(YearEndItem costOfRawMaterials) {
        this.costOfRawMaterials = costOfRawMaterials;
    }

    public YearEndItem getDepreciationAndOther() {
        return depreciationAndOther;
    }

    public void setDepreciationAndOther(YearEndItem depreciationAndOther) {
        this.depreciationAndOther = depreciationAndOther;
    }

    public YearEndItem getOtherIncome() {
        return otherIncome;
    }

    public void setOtherIncome(YearEndItem otherIncome) {
        this.otherIncome = otherIncome;
    }

    public YearEndItem getTax() {
        return tax;
    }

    public void setTax(YearEndItem tax) {
        this.tax = tax;
    }

    public YearEndItem getProfit() {
        return profit;
    }

    public void setProfit(YearEndItem profit) {
        this.profit = profit;
    }

    public YearEndItem getTurnOver() {
        return turnOver;
    }

    public void setTurnOver(YearEndItem turnOver) {
        this.turnOver = turnOver;
    }

    public YearEndItem getStaffCosts() {
        return staffCosts;
    }

    public void setStaffCosts(YearEndItem staffCosts) {
        this.staffCosts = staffCosts;
    }

    public YearEndItem getOtherCharges() {
        return otherCharges;
    }

    public void setOtherCharges(YearEndItem otherCharges) {
        this.otherCharges = otherCharges;
    }
}
