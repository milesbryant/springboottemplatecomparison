package uk.co.crunch;

import java.math.BigDecimal;

/**
 * Created by jgroth on 14/04/16.
 */
public class YearEndItem {
    private BigDecimal balance;
    private BigDecimal comparativeBalance;

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getComparativeBalance() {
        return comparativeBalance;
    }

    public void setComparativeBalance(BigDecimal comparativeBalance) {
        this.comparativeBalance = comparativeBalance;
    }
}
