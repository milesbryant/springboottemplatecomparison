package config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.templateresolver.FileTemplateResolver;
import org.thymeleaf.templateresolver.TemplateResolver;

@Configuration
public class ThymeLeafConfig  {
    
    @Autowired
    private ApplicationContext applicationContext;

    @Value("${use.caching:true}")
    private boolean cachingEnabled;

    @Bean
    public TemplateEngine thymeleafTemplateEngine() {
        TemplateResolver resolver = new FileTemplateResolver();

        resolver.setPrefix(System.getProperty("user.dir") + "/src/main/resources/templates/thymeleaf/");

        resolver.setCacheable(cachingEnabled);
        
        resolver.setTemplateMode("HTML5");
                
        
        SpringTemplateEngine engine = new SpringTemplateEngine();

        System.out.println(engine.getTemplateResolvers());
        
        engine.setTemplateResolver(resolver);
        
        return engine;
    }

    @Bean
    public TemplateResolver thymeleafTemplateResolver() {
        TemplateResolver resolver = new FileTemplateResolver();
        
        resolver.setPrefix(System.getProperty("user.dir") + "/src/main/resources/templates/thymeleaf/");
        
        resolver.setCacheable(false);
        return resolver;
    }
    
}
