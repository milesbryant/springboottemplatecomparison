package config;

import freemarker.cache.FileTemplateLoader;
import freemarker.cache.TemplateLoader;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.resource.ResourceUrlEncodingFilter;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;
import org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver;

import java.io.File;
import java.io.IOException;

@Configuration
public class FreeMarkerConfig {
    
    @Value("${use.caching:true}")
    private boolean cachingEnabled;
    
    
    @Bean
    public freemarker.template.Configuration freeMarkerConfiguration(
            FreeMarkerConfigurer configurer) throws IOException {
        freemarker.template.Configuration configuration = configurer.getConfiguration();
        configuration.setTemplateLoader(freeMarkerTemplateLoader());
        configuration.setDefaultEncoding("UTF-8");
        return configuration;
    }

    @Bean
    public FreeMarkerViewResolver freeMarkerViewResolver() {
        FreeMarkerViewResolver resolver = new FreeMarkerViewResolver();
        resolver.setCache(cachingEnabled);
        return resolver;
    }

    @Bean
    public ResourceUrlEncodingFilter resourceUrlEncodingFilter() {
        return new ResourceUrlEncodingFilter();
    }

    @Bean
    public TemplateLoader freeMarkerTemplateLoader() throws IOException {
        FileTemplateLoader fileTemplateLoader = new FileTemplateLoader(new File(System.getProperty("user.dir")));
        return fileTemplateLoader;
    }

}
