<p id="registrationNumber">
	<ix:nonNumeric name="uk-bus:UKCompaniesHouseRegisteredNumber"
		contextRef="CurrYearDuration">
			Registered number: ${yearEndDocument.companyRegistrationNumber}
		 </ix:nonNumeric>
</p>


<table class="front-page-table">
	<tr>
		<td><ix:nonNumeric
				name="uk-bus:EntityCurrentLegalOrRegisteredName"
				contextRef="CurrYearDuration">
					${yearEndDocument.companyName}
				</ix:nonNumeric></td>
	</tr>

	<#if (today?date > yearEndDocument.yearEndDate?date)  >
        <tr>
            <td>INTERIM REPORT</td>
        </tr>
	</#if>

	<tr>
		<td><ix:nonNumeric name="uk-bus:BusinessReportName" contextRef="CurrYearDuration">
          	  Unaudited Financial Statements
        	</ix:nonNumeric>
		</td>
	</tr>

	<tr>
		<td>for the <#if (today?date > yearEndDocument.yearEndDate?date) > period <#else> year </#if>ended
            <ix:nonNumeric name="uk-bus:EndDateForPeriodCoveredByReport" contextRef="CurrYearEnd" format="ixt:datelonguk">
						${yearEndDocument.yearEndDate?date}
			</ix:nonNumeric>
		</td>
	</tr>

</table>

