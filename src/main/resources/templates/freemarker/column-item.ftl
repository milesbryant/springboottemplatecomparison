<#macro columnItem styleclasses ixbrlTag contextRef format balance ixbrlType>
    <#setting number_format="#,###;(#,###)">
    <td class="${styleclasses}">
        <xbrli:${ixbrlType} name="${ixbrlTag}" contextRef="${contextRef}" unitRef="GBP" decimals="0" format="${format}">
        <#if balance?? >
            <#if balance != 0>
                ${balance}
            <#else>
                -
            </#if>
        <#else>
            &nbsp;
        </#if>
        </xbrli:${ixbrlType}>
    </td>
</#macro>
