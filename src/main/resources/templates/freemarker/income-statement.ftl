<!--
 This income statement uses the FRS 102 iXBRL tags even though we are adhering to the FRS 105 accountancy standard.
 This is because at the time of writing FRS 105 tags are not available.

 See https://uk-taxonomies-tdp.corefiling.com/yeti; select FRS-102 for tag reference.
 See https://www.frc.org.uk/Our-Work/Publications/Accounting-and-Reporting-Policy/FRS-105-The-Financial-Reporting-Standard-applicab.pdf
 for a description of the FRS 105 document format.
-->

<#include "/freemarker/income-statement-row.ftl">

<h1 class="section-title">
    Income Statement
</h1>

<#setting date_format="yyyy">

<table class="front-page-table data-table">
    <tr class="row">
        <td class="data-heading-cell"></td>
        <td class="data-heading-cell">${yearEndDocument.yearEndDate?date}</td>
        <#if yearEndDocument.comparativeEndDate??>
            <td>&#160;</td>
            <td class="data-heading-cell">${yearEndDocument.comparativeEndDate?date}</td>
        </#if>
    </tr>
    <tr class="row">
        <td class="data-heading-cell"></td>
        <td class="data-heading-cell">&#163;</td>
        <#if yearEndDocument.comparativeEndDate?? >
            <td>&#160;</td>
            <td class="data-heading-cell">&#163;</td>
        </#if>
    </tr>

    <@renderRow "Turnover" "uk-frs102:TurnoverRevenue" yearEndDocument.incomeStatement.turnOver yearEndDocument.comparativeEndDate " cell-right" />
    <@renderRow "Other income" "uk-frs102:OtherOperatingIncomeFormat1" yearEndDocument.incomeStatement.otherIncome yearEndDocument.comparativeEndDate " cell-right" />
    <@renderRow "Cost of raw materials and consumables" "uk-frs102:RawMaterialsConsumablesUsed" yearEndDocument.incomeStatement.costOfRawMaterials yearEndDocument.comparativeEndDate " cell-right" />
    <@renderRow "Staff costs" "uk-frs102:StaffCostsEmployeeBenefitsExpense" yearEndDocument.incomeStatement.staffCosts yearEndDocument.comparativeEndDate " cell-right" />
    <@renderRow "Depreciation and other amounts written off assets" "uk-frs102:DepreciationAmortisationImpairmentExpense" yearEndDocument.incomeStatement.depreciationAndOther yearEndDocument.comparativeEndDate " cell-right" />
    <@renderRow "Other charges" "uk-frs102:AdministrativeExpenses" yearEndDocument.incomeStatement.otherCharges yearEndDocument.comparativeEndDate " cell-right" />
    <@renderRow "Tax" "uk-frs102:TaxTaxCreditOnProfitOrLossOnOrdinaryActivities" yearEndDocument.incomeStatement.tax yearEndDocument.comparativeEndDate " cell-right" />
    <@renderRow "Profit or loss" "uk-frs102:ProfitLossOnOrdinaryActivitiesAfterTax" yearEndDocument.incomeStatement.profit yearEndDocument.comparativeEndDate " cell-right double-line income-statement-last-row" />

</table>
