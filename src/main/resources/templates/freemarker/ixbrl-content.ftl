<div class="hide">
    <ix:header>
        <ix:references>
            <link:schemaRef xlink:type="simple" xlink:href="http://xbrl.frc.org.uk/fr/2014-09-01/core/frc-core-full-2014-09-01.xsd" />
        </ix:references>
        <ix:resources>
            <xbrli:context id="CurrYearDuration">
                <xbrli:entity>
                    <xbrli:identifier scheme="http://www.companieshouse.gov.uk/">$object.yearEndDocument.companyRegistrationNumber</xbrli:identifier>
                </xbrli:entity>
                <xbrli:period>
                    <xbrli:startDate>$dateTool.format('yyyy-MM-dd', $object.yearEndDocument.yearStartDate)</xbrli:startDate>
                    <xbrli:endDate>$dateTool.format('yyyy-MM-dd', $object.yearEndDocument.yearEndDate)</xbrli:endDate>
                </xbrli:period>
            </xbrli:context>
            <xbrli:context id="ComparativeYearDuration">
                <xbrli:entity>
                    <xbrli:identifier scheme="http://www.companieshouse.gov.uk/">$object.yearEndDocument.companyRegistrationNumber</xbrli:identifier>
                </xbrli:entity>
                <xbrli:period>
                    <xbrli:startDate>$dateTool.format('yyyy-MM-dd', $object.yearEndDocument.comparativeStartDate)</xbrli:startDate>
                    <xbrli:endDate>$dateTool.format('yyyy-MM-dd', $object.yearEndDocument.comparativeEndDate)</xbrli:endDate>
                </xbrli:period>
            </xbrli:context>
            <xbrli:context id="CurrYearEnd">
                <xbrli:entity>
                    <xbrli:identifier scheme="http://www.companieshouse.gov.uk/">$object.yearEndDocument.companyRegistrationNumber</xbrli:identifier>
                </xbrli:entity>
                <xbrli:period>
                    <xbrli:instant>$dateTool.format('yyyy-MM-dd', $object.yearEndDocument.yearEndDate)</xbrli:instant>
                </xbrli:period>
            </xbrli:context>
            <xbrli:unit id="GBP">
                <xbrli:measure>iso4217:GBP</xbrli:measure>
            </xbrli:unit>

        </ix:resources>
    </ix:header>
</div>
