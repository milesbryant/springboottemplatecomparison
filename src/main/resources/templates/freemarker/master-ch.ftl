<#setting date_format="dd MMMM yyyy">
<#setting locale="en_GB">

<#include "/freemarker/header.xml">

<head>
    <title>Companies House iXbrl</title>
<style type="text/css">
	<#include "/freemarker/style.css">
</style>
</head>
<body>
<#include "/freemarker/cover-sheet.ftl">
<#include "/freemarker/income-statement.ftl">
</body>
</html>
