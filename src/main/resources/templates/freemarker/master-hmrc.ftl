#** ${templates.header} *#


#include("year-end/header.xml")
<head>
	<title>HMRC iXbrl</title>
<style type="text/css">
	#include("year-end/style.css");
</style>
</head>
<body>
	#parse( "year-end/cover-sheet.vm" )
	#parse( "year-end/income-statement.vm" )
	#parse( "year-end/statement-of-financial-position.vm" )
	#parse( "year-end/ixbrl-content.vm" )
</body>
</html>
