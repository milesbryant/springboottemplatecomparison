#**
 *  Splits a table cell into two equal columns.
 *#
#macro(splitTd $content1 $content2)
        <table class="table-split-columns"><tr><td class="data-heading-cell">$content1</td><td class="data-heading-cell">$content2</td></tr></table>
#end

#**
 *  Renders a single row for the Statement of Financial Position page.
 *  $title: Row description, e.g. "Current assets"
 *  $ixbrlTag: UK FRS102/105 iXBRL tag name, e.g. uk-frs102:NetCurrentAssetsLiabilities
 *  $item: the balance to display as an integer, e.g. 140000. This will be formatted.
 *  $comparativeDate: conditionally renders the comparative column, if it evaluates to true.
 *  $position: which of the split columns to render the balance/styling in - can be "left" or "right".
 *  $styleclasses: additional CSS classes to add to the inner table cell. E.g. "double-line"
 *#
#macro(renderSoFPRow $title $ixbrlTag $item $comparativeDate $position $styleclasses)

    #parse("year-end/column-item.vm")
<tr class="row">

    <td class="data-title-cell sofp-row-title">
        $title
    </td>

    #if ( !$comparativeDate )
        <td class="column-spacer">&nbsp;</td>
    #end

    <td>
        ## This cell is split into two columns. To maintain compatibility with PD4ML, we have to use an ugly one row table within
        ## a cell, which in turn has two <td>
        <table class="table-split-columns">
            <tr>
                #if ($position == "left")
                    #columnItem($styleclasses $ixbrlTag "CurrYearDuration" "ixt:numcommadot" $item.balance "monetaryItemType")
                    <td class="cell-center split-cell">&nbsp;</td>
                #else
                    <td class="cell-center split-cell">&nbsp;</td>
                    #columnItem($styleclasses $ixbrlTag "CurrYearDuration" "ixt:numcommadot" $item.balance "monetaryItemType")
                #end
            </tr>
         </table>
    </td>
    #if ( $comparativeDate )
        <td>&#160;</td>
        <td class="data-numeric-cell"
            align="right">
            <table class="table-split-columns">
                <tr>
                    #if ($position == "left")
                        #columnItem($styleclasses $ixbrlTag "ComparativeYearDuration" "ixt:numcommadot" $item.comparativeBalance "monetaryItemType")
                        <td class="cell-center split-cell">&nbsp;</td>
                    #else
                        <td class="cell-center split-cell">&nbsp;</td>
                        #columnItem($styleclasses $ixbrlTag "ComparativeYearDuration" "ixt:numcommadot" $item.comparativeBalance "monetaryItemType")
                    #end
                </tr>
            </table>
        </td>
    #end
</tr>
#end
