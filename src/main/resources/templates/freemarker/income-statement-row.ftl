<#macro renderRow title ixbrlTag item comparativeDate style>
<#include "/freemarker/column-item.ftl">
<tr class="row">
    <td class="data-title-cell">
        ${title}
    </td>
    <@columnItem style ixbrlTag "CurrYearDuration" "ixt:numcommadot" item.balance "nonFraction" />

    <#if comparativeDate?? >
        <td>&#160;</td>
        <@columnItem style ixbrlTag "CurrYearDuration" "ixt:numcommadot" item.comparativeBalance "nonFraction" />
    </#if>
</tr>
</#macro>
