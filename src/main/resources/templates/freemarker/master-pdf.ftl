#** ${templates.header} *#


#include("year-end/header.xml")
<head>
    <title>Year End Document</title>
<style type="text/css">
	<#include "year-end/style.css">
</style>
</head>
<body>
	<#include "year-end/cover-sheet.ftl">
	<p class="page-break" />
	<pd4ml:toc pncorr="-2" />
	<p class="page-break" />
	<#include "year-end/income-statement.vm" >
	<p class="page-break" />
	<#include "year-end/statement-of-financial-position.vm" >
</body>
</html>
