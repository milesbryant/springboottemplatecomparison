#parse("year-end/statement-of-financial-position-row.vm")
<h1 class="section-title">
    Statement of Financial Position
</h1>

<table class="front-page-table data-table">
    <tr class="row">
        <td class="data-heading-cell"></td>
        #if ( !$object.yearEndDocument.comparativeEndDate )
            <td class="column-spacer">&nbsp;</td>
        #end
        <td class="data-heading-cell" colspan="2">$dateTool.getYear($object.yearEndDocument.yearEndDate)</td>
        #if ( $object.yearEndDocument.comparativeEndDate )
            <td class="data-heading-cell" colspan="2">$dateTool.getYear($object.yearEndDocument.comparativeEndDate)</td>
        #end
    </tr>
    <tr class="row">
        <td class="data-heading-cell">&nbsp;</td>
        #if ( !$object.yearEndDocument.comparativeEndDate )
            <td class="column-spacer">&nbsp;</td>
        #end
        <td class="data-heading-cell">
            #splitTd("&#163;" "&#163;")
        </td>
        #if ( $object.yearEndDocument.comparativeEndDate )
            <td>&#160;</td>
            <td class="data-heading-cell">
                #splitTd("&#163;" "&#163;")
            </td>
        #end
    </tr>

    #renderSoFPRow("Called up share capital not paid" "uk-frs102:CalledUpShareCapitalNotPaidNotExpressedAsCurrentAsset" $object.yearEndDocument.statementOfFinancialPosition.calledUpShareCapitalNotPaid $object.yearEndDocument.comparativeEndDate "right" "cell-center split-cell")
    #renderSoFPRow("Fixed assets" "uk-frs102:FixedAssets" $object.yearEndDocument.statementOfFinancialPosition.fixedAssets $object.yearEndDocument.comparativeEndDate "right" "cell-center split-cell")
    #renderSoFPRow("Current assets" "uk-frs102:CurrentAssets" $object.yearEndDocument.statementOfFinancialPosition.currentAssets $object.yearEndDocument.comparativeEndDate "left" "cell-center split-cell")
    #renderSoFPRow("Prepayments and accrued income" "uk-frs102:PrepaymentsAccruedIncomeNotExpressedWithinCurrentAssetSubtotal" $object.yearEndDocument.statementOfFinancialPosition.prepaymentsAndAccruedIncome $object.yearEndDocument.comparativeEndDate "left" "cell-center split-cell")
    #renderSoFPRow("Creditors: amounts falling due within one year" "uk-frs102:" $object.yearEndDocument.statementOfFinancialPosition.creditorsAmountsFallingDueWithinOneYear $object.yearEndDocument.comparativeEndDate "left" "cell-center split-cell underline")
    #renderSoFPRow("Net current assets (liabilities)" "uk-frs102:NetCurrentAssetsLiabilities" $object.yearEndDocument.statementOfFinancialPosition.netCurrentAssets $object.yearEndDocument.comparativeEndDate "right" "cell-center split-cell underline")
    #renderSoFPRow("Total assets less current liabilities" "uk-frs102:TotalAssetsLessCurrentLiabilities" $object.yearEndDocument.statementOfFinancialPosition.totalAssetsLessCurrentLiabilities $object.yearEndDocument.comparativeEndDate "right" "cell-center split-cell")
    #renderSoFPRow("Creditors: amounts falling due after more than one year" "uk-frs102:" $object.yearEndDocument.statementOfFinancialPosition.creditorsAmountsFallingDueAfterMoreThanOneYear $object.yearEndDocument.comparativeEndDate "right" "cell-center split-cell")
    #renderSoFPRow("Provisions for liabilities" "uk-frs102:ProvisionsForLiabilitiesBalanceSheetSubtotal" $object.yearEndDocument.statementOfFinancialPosition.provisionsForLiabilities $object.yearEndDocument.comparativeEndDate "right" "cell-center split-cell")
    #renderSoFPRow("Accruals and deferred income" "uk-frs102:AccruedLiabilitiesDeferredIncome" $object.yearEndDocument.statementOfFinancialPosition.accrualsAndDeferredIncome $object.yearEndDocument.comparativeEndDate "right" "cell-center split-cell")
    #renderSoFPRow("Net assets" "uk-frs102:NetAssetsLiabilities" $object.yearEndDocument.statementOfFinancialPosition.netAssets $object.yearEndDocument.comparativeEndDate "right" "cell-center split-cell double-line")
    <tr class="row"><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
    #renderSoFPRow("Capital and reserves" "uk-frs102:Equity" $object.yearEndDocument.statementOfFinancialPosition.capitalAndReserves $object.yearEndDocument.comparativeEndDate "right" "cell-center split-cell double-line")
</table>
